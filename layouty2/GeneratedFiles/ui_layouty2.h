/********************************************************************************
** Form generated from reading UI file 'layouty2.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LAYOUTY2_H
#define UI_LAYOUTY2_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_layouty2Class
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QLineEdit *lineEdit;
    QLabel *label;
    QFormLayout *formLayout;
    QPushButton *pushButton;
    QLabel *label_2;
    QListWidget *listWidget;
    QSpacerItem *verticalSpacer;
    QFormLayout *formLayout_2;
    QPushButton *pushButton_2;
    QComboBox *comboBox;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *layouty2Class)
    {
        if (layouty2Class->objectName().isEmpty())
            layouty2Class->setObjectName(QStringLiteral("layouty2Class"));
        layouty2Class->resize(381, 443);
        centralWidget = new QWidget(layouty2Class);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout_2 = new QVBoxLayout(centralWidget);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(-1, -1, -1, 0);
        lineEdit = new QLineEdit(centralWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        verticalLayout->addWidget(lineEdit);

        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout->addWidget(label);

        formLayout = new QFormLayout();
        formLayout->setSpacing(6);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setContentsMargins(-1, -1, -1, 0);
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        formLayout->setWidget(0, QFormLayout::LabelRole, pushButton);

        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(0, QFormLayout::FieldRole, label_2);


        verticalLayout->addLayout(formLayout);


        verticalLayout_2->addLayout(verticalLayout);

        listWidget = new QListWidget(centralWidget);
        listWidget->setObjectName(QStringLiteral("listWidget"));

        verticalLayout_2->addWidget(listWidget);

        verticalSpacer = new QSpacerItem(20, 80, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);

        formLayout_2 = new QFormLayout();
        formLayout_2->setSpacing(6);
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        formLayout_2->setContentsMargins(-1, -1, -1, 0);
        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, pushButton_2);

        comboBox = new QComboBox(centralWidget);
        comboBox->setObjectName(QStringLiteral("comboBox"));

        formLayout_2->setWidget(0, QFormLayout::FieldRole, comboBox);


        verticalLayout_2->addLayout(formLayout_2);

        layouty2Class->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(layouty2Class);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 381, 21));
        layouty2Class->setMenuBar(menuBar);
        mainToolBar = new QToolBar(layouty2Class);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        layouty2Class->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(layouty2Class);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        layouty2Class->setStatusBar(statusBar);

        retranslateUi(layouty2Class);

        QMetaObject::connectSlotsByName(layouty2Class);
    } // setupUi

    void retranslateUi(QMainWindow *layouty2Class)
    {
        layouty2Class->setWindowTitle(QApplication::translate("layouty2Class", "layouty2", 0));
        label->setText(QApplication::translate("layouty2Class", "label", 0));
        pushButton->setText(QApplication::translate("layouty2Class", "PushButton", 0));
        label_2->setText(QApplication::translate("layouty2Class", "TextLabel", 0));
        pushButton_2->setText(QApplication::translate("layouty2Class", "PushButton", 0));
    } // retranslateUi

};

namespace Ui {
    class layouty2Class: public Ui_layouty2Class {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LAYOUTY2_H
