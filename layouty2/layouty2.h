#ifndef LAYOUTY2_H
#define LAYOUTY2_H

#include <QtWidgets/QMainWindow>
#include "ui_layouty2.h"

class layouty2 : public QMainWindow
{
	Q_OBJECT

public:
	layouty2(QWidget *parent = 0);
	~layouty2();

private:
	Ui::layouty2Class ui;
};

#endif // LAYOUTY2_H
